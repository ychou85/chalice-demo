import json
import boto3
import pytz
import base64
import uuid
import urlparse
import subprocess
# chalice doesn't support sub module imports!
# import settings as settings
import datetime as dt
from botocore.exceptions import ClientError
from chalice import Chalice, Response

app = Chalice(app_name='helloworld')
app.debug = True


@app.route('/')
def index():
    return {'hello': 'world'}


@app.route('/status')
def status():
    current_datetime = dt.datetime.now(pytz.timezone("America/New_York"))
    fmt = '%Y-%m-%d %H:%M:%S %Z'
    current_datetime_string = current_datetime.strftime(fmt)
    ret_response = {'status': 'Operational',
                    'time': current_datetime_string
                    }
    try:
        # attempt to connect to S3 bucket
        S3_service = boto3.resource('s3')
        bucket = S3_service.Bucket('mybucket-yh-chou')
        if not bucket:
            ret_response['status'] = 'S3 bucket is unreachable!'
    except Exception as e:
        ret_response['status'] = 'S3 is unreachable! ' + str(e)
        return ret_response

    return ret_response


@app.route('/dev/upload', methods=['PUT', 'POST'],
           content_types=['application/json'], cors=True)
def upload():
    S3_BUCKET = 'mybucket-yh-chou'
    ret_response = {'status': 'Upload Successful!'}
    request_dict = app.current_request.to_dict()
    ret_response['request'] = request_dict

    try:
        S3_service = boto3.client('s3')

        body = app.current_request.json_body
        data = body['data']
        if not data:
            ret_response['status'] = 'Server Error'
            return ret_response

        if len(data) % 4:
            # not a multiple of 4, add padding:
            data += '=' * (4 - len(data) % 4) 
        image = base64.b64decode(data)
        mode = {'max': '', 'min': '^', 'exact': '!'}[body.get('mode', 'max').lower()]
        width = int(body.get('width', 128))
        height = int(body.get('height', 128))
        img_format = body.get('format', 'png').lower()
        if img_format != 'png':
            ret_response['status'] = 'Sorry, we only accept png'
            return ret_response

        cmd = [
            'convert',  # ImageMagick Convert
            '-',  # Read original picture from StdIn
            '-auto-orient',  # Detect picture orientation from metadata
            '-thumbnail', '{}x{}{}'.format(width, height, mode),  # Thumbnail size
            '-extent', '{}x{}'.format(width, height),  # Fill if original picture is smaller than thumbnail
            '-gravity', 'Center',  # Extend (fill) from the thumbnail middle
            '-unsharp',' 0x.5',  # Un-sharpen slightly to improve small thumbnails
            '-quality', '80%',  # Thumbnail JPG quality
            '{}:-'.format(img_format),  # Write thumbnail with `format` to StdOut
        ]

        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stdin=subprocess.PIPE)
        thumbnail = p.communicate(input=image)[0]

        if not thumbnail:
            ret_response['status'] = 'Image format not supported'
            return ret_response

        filename = '%(uid)s.%(fmt)s' % {'uid': uuid.uuid4(),
                                        'fmt': img_format
                                        }
        S3_service.put_object(
            Bucket=S3_BUCKET,
            Key=filename,
            Body=thumbnail,
            ACL='public-read',
            ContentType='image/%(fmt)s' % {'fmt': format},
        )

        return {
            'url': 'https://s3.amazonaws.com/%(bucket)s/%(filename)s' % {'bucket': S3_BUCKET, 'filename': filename}
        }
    except Exception as e:
        ret_response['status'] = 'Server Error' + str(e)
        return ret_response

    return ret_response


# The view function above will return {"hello": "world"}
# whenever you make an HTTP GET request to '/'.
#
# Here are a few more examples:
#
# @app.route('/hello/{name}')
# def hello_name(name):
#    # '/hello/james' -> {"hello": "james"}
#    return {'hello': name}
#
# @app.route('/users', methods=['POST'])
# def create_user():
#     # This is the JSON body the user sent in their POST request.
#     user_as_json = app.json_body
#     # Suppose we had some 'db' object that we used to
#     # read/write from our database.
#     # user_id = db.create_user(user_as_json)
#     return {'user_id': user_id}
#
# See the README documentation for more examples.
#
